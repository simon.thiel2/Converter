package com.cuupa.converter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@EnableAutoConfiguration
public class SpringBootConverterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootConverterApplication.class, args);
	}
}
