package com.cuupa.converter.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cuupa.converter.services.Converter;
import com.cuupa.converter.to.Document;
import com.google.gson.Gson;

@RestController
public class ConvertController {

	private Converter converter;

	private static final Gson gson = new Gson();
	
	public ConvertController(Converter converter) {
		this.converter = converter;
	}

	@RequestMapping("/ping")
	public ResponseEntity<String> ping() {
		return ResponseEntity.ok().body("200");
	}

	@RequestMapping("/convert")
	public ResponseEntity<String> convert(@RequestBody String json) {

		try {
			Document document = gson.fromJson(json, Document.class);
			document = converter.convert(document);
			String result = gson.toJson(document);
			return ResponseEntity.ok(result);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
	}
}
