package com.cuupa.converter.configuration.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cuupa.converter.controller.ConvertController;
import com.cuupa.converter.services.Converter;

@Configuration
public class ApplicationConfiguration {

	@Bean
	public Converter converter() {
		return new Converter();
	}

	@Bean
	public ConvertController convertController() {
		return new ConvertController(converter());
	}
}
