package com.cuupa.converter.to.eml.parser;

import javax.mail.internet.ContentType;

public class MimeObjectEntry<T> {
	private T entry;
	private ContentType contentType;

	public MimeObjectEntry(T entry, ContentType contentType) {
		this.entry = entry;
		this.contentType = contentType;
	}

	public T getEntry() {
		return entry;
	}

	public void setEntry(T entry) {
		this.entry = entry;
	}

	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}
}