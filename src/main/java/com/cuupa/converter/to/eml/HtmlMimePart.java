package com.cuupa.converter.to.eml;

import javax.mail.internet.ContentType;

public class HtmlMimePart {

	private String content;
	
	private ContentType contentType;

	public void setContent(String content, ContentType contentType) {
		this.content = content;
		this.contentType = contentType;
	}

	public String getContent() {
		return content;
	}

	public ContentType getContentType() {
		return contentType;
	}
}
