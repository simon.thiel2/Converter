package com.cuupa.converter.to.eml.parser;

public enum MimeType {

    TEXT("text/*"), TEXT_PLAIN("text/plain"), TEXT_HTML("text/html"), MULTIPART("multipart/*"), IMAGE("image/*");

    private final String value;

    private MimeType(String value) {
        this.value = value;
    }

    /**
     * @return the value
     */
    public String get() {
        return value;
    }
}