package com.cuupa.converter.to.eml.parser;

import javax.mail.Part;

public interface MimeParserCallback {

	void callback(Part part) throws Exception;

}
