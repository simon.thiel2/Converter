package com.cuupa.converter.to.eml;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.mail.Address;
import javax.mail.internet.ContentType;

public class Email {

    private String sender;

    private Address[] recipients;

    private String subject;

    private LocalDateTime receiveTime;

    private final PlainTextMimePart plaintextMimePart = new PlainTextMimePart();

    private final HtmlMimePart htmlMimePart = new HtmlMimePart();

    private final List<Attachment> attachments = new ArrayList<>();

	private boolean _lotusNotesMessage;

    /**
     * @return the sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * @param sender the sender to set
     */
    public void setSender(String sender) {
        this.sender = _decodeText(sender);
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = _decodeText(subject);
    }

    public void setPlainText(String content, ContentType contentType) {
        plaintextMimePart.setContent(content, contentType);
    }

    public void setHtmlText(String content, ContentType contentType) {
        htmlMimePart.setContent(content, contentType);
    }

    public void addAttachment(Attachment attachment) {
        attachments.add(attachment);
    }

    /**
     * @return
     */
    public String getPlainText() {
        return plaintextMimePart.getContent();
    }
    
    public String getHtmlText() {
    	return htmlMimePart.getContent();
    }

    /**
     * @return
     */
    public Address[] getRecipients() {
        return recipients;
    }

    /**
     * @param recipients
     */
    public void setRecipients(Address[] recipients) {
        this.recipients = recipients;
    }

    /**
     * @param i
     * @return
     */
    public String getRecipient(int index) {
        if (recipients.length >= index) {
            return recipients[index].toString();
        }
        return null;
    }

    /**
     * @param localDate
     */
    public void setReceiveTime(LocalDateTime receiveTime) {
        this.receiveTime = receiveTime;
    }

    /**
     * @return the receiveTime
     */
    public LocalDateTime getReceiveTime() {
        return receiveTime;
    }

    /**
     * @return
     */
    public List<Attachment> getAttachments() {
        return attachments;
    }
    
    /**
     * This sets a flag for the proprietary Lotus Notes encoding for special characters like 'ä', 'ü' which is encoded as
     * '0xL184z' and '0xL181z' respectivly
     * @param lotusNotesMessage The parameter wether it's a Lotus Notes message or not
     */
    public void _setLotusNotesEncodingFix(boolean _lotusNotesMessage) {
        this._lotusNotesMessage = _lotusNotesMessage;
    }

    /**
     * If this message is a document from Lotus Notes, then there could be a possibility of mailheaders containing incorrect
     * decoded special characters TODO: There are definitely more special characters than 'ä', 'ö' and 'ü' ...
     * @param text The text to decode
     * @return The decoded text
     */
    private String _decodeText(String text) {
        if (_lotusNotesMessage) {
           return text.replace("0xL184z", "ä").replace("0xL181z", "ü").replace("0xL194z", "ö");
        }
        return text;
    }
}