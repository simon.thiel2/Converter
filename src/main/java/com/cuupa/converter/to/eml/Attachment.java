package com.cuupa.converter.to.eml;

import javax.mail.internet.ContentType;

public class Attachment {

	private final String fileName;

    private final byte[] content;

    private final ContentType contentType;

    public Attachment(String fileName, byte[] content, ContentType contentType) {
        this.fileName = fileName;
        this.content = content;
        this.contentType = contentType;
    }

    /**
     * @return
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @return the contentType
     */
    public ContentType getContentType() {
        return contentType;
    }
}
