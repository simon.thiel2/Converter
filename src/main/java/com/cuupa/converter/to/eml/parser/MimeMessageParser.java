package com.cuupa.converter.to.eml.parser;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;

import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.commons.lang3.StringUtils;

import com.cuupa.converter.to.eml.Attachment;
import com.cuupa.converter.to.eml.Email;
import com.google.common.io.BaseEncoding;
import com.google.common.io.ByteStreams;
import com.sun.mail.util.BASE64DecoderStream;

public class MimeMessageParser {

    // html wrapper template for text/plain messages
    private static final String HTML_WRAPPER_TEMPLATE =
        "<!DOCTYPE html><html><head><style>body{font-size: 0.5cm;}</style><meta charset=\"%s\"><title>%s</title></head><body>%s</body></html>";

    public static Email parse(Message message) throws Exception {
        Email email = new Email();
        email._setLotusNotesEncodingFix(isLotusNotesMessage(message));

        email.setRecipients(message.getRecipients(RecipientType.TO));
        email.setSubject(message.getSubject());

        String[] dateHeaders = message.getHeader("Date");
        for (int i = 0; i < dateHeaders.length; i++) {
            String date = removeTimeZoneInfo(dateHeaders[i]);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E, d MMM y HH:mm:ss Z", Locale.ENGLISH);
            LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
            email.setReceiveTime(dateTime);
        }

        if (message.getFrom() != null && message.getFrom().length > 0) {
            email.setSender(message.getFrom()[0].toString());
        }

        HashMap<String, MimeObjectEntry<String>> inlineImageMap = getInlineImageMap(message);

        parseMimeStructure(message, new MimeParserCallback() {

            @Override
            public void callback(Part part) throws Exception {

                boolean isAttachment = Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition());

                if (!isValidPart(part) && !isAttachment) {
                    return;
                }

                if (!isAttachment) {
                    String content = getStringContent(part);
                    ContentType contentType = new ContentType(part.getContentType());

                    if (StringUtils.isNotBlank(content)) {
                        if (part.isMimeType(MimeType.TEXT_PLAIN.get())) {
                            email.setPlainText(content, contentType);
                        } else if (part.isMimeType(MimeType.TEXT_HTML.get())) {
                            content = parseHTML(content, email.getSubject(), contentType, inlineImageMap);
                            email.setHtmlText(content, contentType);
                        }
                    }
                } else {
                    String fileName = part.getFileName();
                    if (StringUtils.isNotBlank(fileName)) {
                        try (InputStream stream = part.getInputStream()) {
                            byte[] content = new byte[part.getSize()];
                            stream.read(content);
                            email.addAttachment(new Attachment(fileName, content, new ContentType(part.getContentType())));
                        }
                    }
                }
            }
        });
        return email;
    }
    
    /**
     * Determine if this message is a Lotus Notes Message for special character encoding
     * @param message The Message
     * @return true if the message is a Lotus Notes message, false otherwise
     */
    private static boolean isLotusNotesMessage(Message message) {
        try {
            Enumeration<Header> allHeaders = message.getAllHeaders();

            while (allHeaders.hasMoreElements()) {
                Header header = allHeaders.nextElement();
                if (header.getName().equalsIgnoreCase("X-Notes-Item")) {
                    return true;
                }
            }
        } catch (MessagingException e) {
            return false;
        }
        return false;
    }

    /**
     * @param string
     * @return
     */
    private static String removeTimeZoneInfo(String date) {
        if (date.contains("(") && date.contains(")")) {
            return date.split("\\(")[0].trim();
        }
        return date;
    }

    private static void parseMimeStructure(Part part, MimeParserCallback callback) throws Exception {
        callback.callback(part);

        if (part.isMimeType(MimeType.MULTIPART.get())) {
            Multipart mp = (Multipart) part.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                parseMimeStructure(mp.getBodyPart(i), callback);
            }
        }
    }
    
    private static boolean isValidPart(Part part) {
		try {
			return part != null && (part.isMimeType(MimeType.TEXT.get()) || part.isMimeType(MimeType.MULTIPART.get()));
		} catch (MessagingException e) {
			return false;
		}
	}

    /**
     * Get the String Content of a MimePart.
     * @param p MimePart
     * @return Content as String
     * @throws IOException
     * @throws MessagingException
     */
    private static String getStringContent(Part part) throws IOException, MessagingException {
        Object content = null;

        try {
            content = part.getContent();
        } catch (Exception e) {
            // most likely the specified charset could not be found
            content = part.getInputStream();
        }

        String stringContent = null;

        if (content instanceof String) {
            stringContent = (String) content;
        } else if (content instanceof InputStream) {
            ContentType contentType = new ContentType(part.getContentType());
            stringContent = new String(ByteStreams.toByteArray((InputStream) content), contentType.getParameter("charset"));
        } 
        return stringContent;
    }

    /**
     * Get all inline images (images with an Content-Id) as a Hashmap. The key is the Content-Id and all images in all multipart
     * containers are included in the map.
     * @param p mime object
     * @return Hashmap&lt;Content-Id, &lt;Base64Image, ContentType&gt;&gt;
     * @throws Exception
     */
    private static HashMap<String, MimeObjectEntry<String>> getInlineImageMap(Part part) throws Exception {
        final HashMap<String, MimeObjectEntry<String>> result = new HashMap<String, MimeObjectEntry<String>>();

        parseMimeStructure(part, new MimeParserCallback() {

            @Override
            public void callback(Part part) throws Exception {
                if (part.isMimeType(MimeType.IMAGE.get()) && (part.getHeader("Content-Id") != null)) {
                    String id = part.getHeader("Content-Id")[0];

                    BASE64DecoderStream b64ds = (BASE64DecoderStream) part.getContent();
                    String imageBase64 = BaseEncoding.base64().encode(ByteStreams.toByteArray(b64ds));
                    result.put(id, new MimeObjectEntry<String>(imageBase64, new ContentType(part.getContentType())));
                }
            }
        });

        return result;
    }

    private static String parseHTML(String content, final String subject, final ContentType contentType,
            HashMap<String, MimeObjectEntry<String>> inlineImageMap) {
        if (contentType.match(MimeType.TEXT_HTML.get())) {
            if (inlineImageMap.size() > 0) {

                // content = StringReplacer.replace(content, IMG_CID_REGEX, new StringReplacerCallback() {
                // @Override
                // public String replace(Matcher m) throws Exception {
                // MimeObjectEntry<String> base64Entry = inlineImageMap.get("<" + m.group(1) + ">");
                //
                // // found no image for this cid, just return the matches string as it is
                // if (base64Entry == null) {
                // return m.group();
                // }
                //
                // return "data:" + base64Entry.getContentType().getBaseType() + ";base64," + base64Entry.getEntry()
                // + "\"";
                // }
                // });
            }
        } else {

            // replace \n line breaks with <br>
            content = content.replace("\n", "<br>").replace("\r", "");

            // replace whitespace with &nbsp;
            content = content.replace(" ", "&nbsp;");

            content = String.format(HTML_WRAPPER_TEMPLATE, contentType.getParameter("charset"), subject, content);
            // if (inlineImageMap.size() > 0) {
            //
            // // find embedded images and embed them in html using <img src="data:image ...>
            // // syntax
            // content = StringReplacer.replace(content, IMG_CID_PLAIN_REGEX, new StringReplacerCallback() {
            //
            // @Override
            // public String replace(Matcher m) throws Exception {
            // MimeObjectEntry<String> base64Entry = inlineImageMap.get("<" + m.group(1) + ">");
            //
            // // found no image for this cid, just return the matches string
            // if (base64Entry == null) {
            // return m.group();
            // }
            //
            // return "<img src=\"data:" + base64Entry.getContentType().getBaseType() + ";base64,"
            // + base64Entry.getEntry() + "\" />";
            // }
            //
            // });
            // }
        }
        return content;
    }

//    public static List<Part> getAttachments(Part p) throws Exception {
//        final List<Part> result = new ArrayList<Part>();
//
//        // walkMimeStructure(p, 0, new WalkMimeCallback() {
//        // @Override
//        // public void walkMimeCallback(Part p, int level) throws Exception {
//        // if (Part.ATTACHMENT.equalsIgnoreCase(p.getDisposition())
//        // || ((p.getDisposition() == null) && !Strings.isNullOrEmpty(p.getFileName())))
//        // {
//        // result.add(p);
//        // }
//        // }
//        // });
//
//        return result;
//    }
}