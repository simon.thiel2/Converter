package com.cuupa.converter.services.command;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

public class WordDocument implements AutoCloseable {

	private static final String WORD_VBA_DATA_XML = "/word/vbaData.xml";
	private static final String APPLICATION_VND_MS_WORD_VBA_DATA_XML = "application/vnd.ms-word.vbaData+xml";
	private static final String PART_NAME = "PartName";
	private static final String CONTENT_TYPE = "ContentType";
	private static final String CONTENT_TYPES_XML = "[Content_Types].xml";
	
	private final String dir = String.valueOf(System.currentTimeMillis());
	
	private byte[] content;

	public static WordDocument loadDocument(com.cuupa.converter.to.Document document) {
		return new WordDocument(document.getContent());
	}
	
	private WordDocument(byte[] content) {
		this.content = content;
	}

	public byte[] removeMakros() throws IOException {
		extractZipToDisk(dir);
		removeFromContentTypes(dir);
		removeVBData(dir);

		byte[] content = persist(dir);

		deleteTempFiles(dir);
		return content;
	}

	private void deleteTempFiles(String dir) {
		deleteDir(dir);
		new File(dir).delete();
	}

	private void deleteDir(String dir) {
		for (File file : new File(dir).listFiles()) {
			if (file.isDirectory()) {
				deleteDir(file.getAbsolutePath());
				file.delete();
			} else {
				file.delete();
			}
		}
	}

	private byte[] persist(String dir) throws IOException {

		List<String> fileList = new ArrayList<>();

		generateFileList(new File(dir), dir, fileList);

		byte[] content = new byte[0];
		
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream(); ZipOutputStream out = new ZipOutputStream(bos)) {
			for (String _file : fileList) {
				byte[] buffer = new byte[1024];
				ZipEntry entry = new ZipEntry(_file);
				out.putNextEntry(entry);
				try (FileInputStream in = new FileInputStream(dir + File.separator + _file)) {
					int length;
					while ((length = in.read(buffer)) > 0) {
						out.write(buffer, 0, length);
					}
				}
			}
			content = bos.toByteArray();
			out.closeEntry();
		}
		return content;
	}

	private void generateFileList(File file, String dir, List<String> value) {

		if (file.isFile()) {
			value.add(generateZipEntry(file.toString(), dir));
		} else if (file.isDirectory()) {
			for (File string : file.listFiles()) {
				generateFileList(string, dir, value);
			}
		}
	}

	private String generateZipEntry(String entry, String dir) {
		return entry.substring(entry.indexOf(File.separator) + 1, entry.length());
	}

	private void removeVBData(String dir) {
		File word = new File(dir, "word");
		File[] listFiles = word.listFiles();
		for (File file : listFiles) {
			if (file.getName().equals("vbaData.xml") || file.getName().equals("vbaProject.bin")) {
				file.delete();
			}
		}
	}

	private void removeFromContentTypes(String dir) throws FileNotFoundException, IOException {
		File file = new File(dir, CONTENT_TYPES_XML);

		SAXBuilder builder = new SAXBuilder();
		try {
			Document document = builder.build(file);
			Element rootElement = document.getRootElement();
			List<Element> newChildren = new ArrayList<>();
			for (Element element : rootElement.getChildren()) {
				if (_isAllowedChild(element)) {
					newChildren.add(element);
				}
			}

			rootElement.setContent(newChildren);

			XMLOutputter xmlOutputter = new XMLOutputter();
			try (OutputStream out = new FileOutputStream(file)) {
				xmlOutputter.output(document, out);
			}
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean _isAllowedChild(Element element) {
		return !APPLICATION_VND_MS_WORD_VBA_DATA_XML.equals(element.getAttributeValue(CONTENT_TYPE))
				&& !WORD_VBA_DATA_XML.equals(element.getAttributeValue(PART_NAME));
	}

	private void extractZipToDisk(String dir) throws IOException, FileNotFoundException {

		byte[] buffer = new byte[1024];

		try (ZipInputStream in = new ZipInputStream(new ByteArrayInputStream(content))) {
			ZipEntry zipEntry = null;
			while ((zipEntry = in.getNextEntry()) != null) {
				String fileName = zipEntry.getName();
				File newFile = new File(dir, fileName);
				newFile.getParentFile().mkdirs();
				if (zipEntry.isDirectory()) {
					newFile.mkdir();
					continue;
				}

				try (FileOutputStream fos = new FileOutputStream(newFile)) {
					int len;
					while ((len = in.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}
				}
			}
		}
	}

	@Override
	public void close() throws Exception {
		deleteTempFiles(dir);
	}
}
