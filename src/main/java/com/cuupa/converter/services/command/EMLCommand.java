package com.cuupa.converter.services.command;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.cuupa.converter.to.eml.Email;
import com.cuupa.converter.to.eml.parser.MimeMessageParser;
import com.cuupa.converter.to.Document;
import com.cuupa.converter.to.DocumentBuilder;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.html.HtmlEscapers;
import com.google.common.io.Files;
import com.google.common.io.Resources;
import com.lowagie.text.DocumentException;

public class EMLCommand extends Command {

//	private static final String ADD_HEADER_IFRAME_JS_TAG_TEMPLATE = "<script id=\"header-v6a8oxpf48xfzy0rhjra\" data-file=\"%s\" type=\"text/javascript\">%s</script>";
	private static final String HEADER_FIELD_TEMPLATE = "<tr><td class=\"header-name\">%s</td><td class=\"header-value\">%s</td></tr>";

	private static final Pattern IMG_CID_PLAIN_REGEX = Pattern.compile("\\[cid:(.*?)\\]", Pattern.DOTALL);
	private static final Pattern IMG_CID_REGEX = Pattern.compile("cid:(.*?)\"", Pattern.DOTALL);
	
	private final Document document;

	public EMLCommand(Document document) {
		this.document = document;
	}

	@Override
	public Document execute() throws Exception {
		MimeMessage message = null;
		
		try (InputStream inputStream = new ByteArrayInputStream(document.getContent())) {
			message = new MimeMessage(null, inputStream);
		}

		Email email = MimeMessageParser.parse(message);
//		String[] recipients = getRecipients(message);
//		String sentDateStr = message.getHeader("date", null);
//		String subject = message.getSubject();
//		String from = message.getHeader("From", null);
//		createHeader(recipients, sentDateStr, subject, from);

		String htmlBody = email.getHtmlText();

		File tmpXhtml = new File(document.getFileName().replace(".eml", ".xhtml"));
		tmpXhtml.getParentFile().mkdirs();
		
		try (InputStream in = new ByteArrayInputStream(htmlBody.getBytes());
				OutputStream out = new FileOutputStream(tmpXhtml)) {
			parseDocument(in, out);
		}

		File pdfFile = render(tmpXhtml);
		
		Document cleanedDocument = null;
		try(InputStream in = new FileInputStream(pdfFile)) {
			cleanedDocument = DocumentBuilder.create(in, pdfFile.getName(), (int)pdfFile.length()).build();
		}
		tmpXhtml.delete();
		pdfFile.delete();
		return cleanedDocument;
	}

	private File render(File tmpXhtml) throws FileNotFoundException, IOException, DocumentException {
		File pdfFile = new File(document.getFileName().replace(".eml", ".pdf"));
		OutputStream os = new FileOutputStream(pdfFile);
		ITextRenderer renderer = new ITextRenderer();
		renderer.setDocument(tmpXhtml);
		renderer.layout();
		renderer.createPDF(os);
		renderer.finishPDF();
		os.close();
		return pdfFile;
	}

	private String[] getRecipients(MimeMessage message) throws MessagingException {
		String[] recipients = new String[0];
		String recipientsRaw = message.getHeader("To", null);
		if (recipientsRaw != null && recipientsRaw.length() > 0) {
			try {
				recipientsRaw = MimeUtility.unfold(recipientsRaw);
				recipients = recipientsRaw.split(",");
				for (int i = 0; i < recipients.length; i++) {
					recipients[i] = MimeUtility.decodeText(recipients[i]);
				}
			} catch (Exception e) {
			}
		}
		return recipients;
	}

	private void createHeader(String[] recipients, String sentDateStr, String subject, String from) throws IOException {
		File tmpHtmlHeader;
		// if (!hideHeaders) {
		tmpHtmlHeader = File.createTempFile("emailtopdf", ".html");
		String tmpHtmlHeaderStr = Resources.toString(Resources.getResource("header.html"), StandardCharsets.UTF_8);
		String headers = "";

		if (!Strings.isNullOrEmpty(from)) {
			headers += String.format(HEADER_FIELD_TEMPLATE, "From", HtmlEscapers.htmlEscaper().escape(from));
		}

		if (!Strings.isNullOrEmpty(subject)) {
			headers += String.format(HEADER_FIELD_TEMPLATE, "Subject",
					"<b>" + HtmlEscapers.htmlEscaper().escape(subject) + "<b>");
		}

		if (recipients.length > 0) {
			headers += String.format(HEADER_FIELD_TEMPLATE, "To",
					HtmlEscapers.htmlEscaper().escape(Joiner.on(", ").join(recipients)));
		}

		if (!Strings.isNullOrEmpty(sentDateStr)) {
			headers += String.format(HEADER_FIELD_TEMPLATE, "Date", HtmlEscapers.htmlEscaper().escape(sentDateStr));
		}

		Files.write(String.format(tmpHtmlHeaderStr, headers), tmpHtmlHeader, StandardCharsets.UTF_8);

		// Append this script tag dirty to the bottom
		// htmlBody += String.format(ADD_HEADER_IFRAME_JS_TAG_TEMPLATE,
		// tmpHtmlHeader.toURI(),
		// Resources.toString(Resources.getResource("contentScript.js"),
		// StandardCharsets.UTF_8));
		// }
	}

	private void parseDocument(InputStream inputStream, OutputStream outputStream) {
		Tidy tidy = new Tidy();
		tidy.setQuiet(false);
		tidy.setShowWarnings(true);
		tidy.setShowErrors(0);
		tidy.setMakeClean(true);
		tidy.setForceOutput(true);

		tidy.setXHTML(true);
		//// tidy.setXmlTags(true);
		tidy.setInputEncoding("UTF-8");
//		tidy.setOutputEncoding("UTF-8");
		tidy.parseDOM(inputStream, outputStream);
	}

}
