package com.cuupa.converter.services.command;

import com.cuupa.converter.to.Document;

public class CommandExecuter {

	public static Document execute(Command command) throws Exception {
		if (command != null) {
			return command.execute();
		}
		return null;
	}
}
