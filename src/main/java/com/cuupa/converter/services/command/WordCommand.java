package com.cuupa.converter.services.command;

import com.cuupa.converter.to.Document;

public class WordCommand extends Command {

	private Document document;

	public WordCommand(Document document) {
		this.document = document;
	}

	@Override
	public Document execute() throws Exception {
		try(WordDocument doc = WordDocument.loadDocument(document)) {
			document.setContent(doc.removeMakros());
		}
		return document;
	}

}
