package com.cuupa.converter.services.command;

import com.cuupa.converter.to.Document;

public abstract class Command {
	
	public abstract Document execute() throws Exception;

}
