package com.cuupa.converter.services.command;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentNameDictionary;
import org.apache.pdfbox.pdmodel.PDEmbeddedFilesNameTreeNode;
import org.apache.pdfbox.pdmodel.common.filespecification.PDComplexFileSpecification;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.pdmodel.interactive.action.PDAction;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionJavaScript;
import org.apache.pdfbox.pdmodel.interactive.action.PDActionMovie;
import org.apache.pdfbox.pdmodel.interactive.action.PDDocumentCatalogAdditionalActions;
import org.apache.pdfbox.pdmodel.interactive.action.PDFormFieldAdditionalActions;
import org.apache.pdfbox.pdmodel.interactive.annotation.PDAnnotationWidget;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.pdmodel.interactive.form.PDNonTerminalField;
import org.apache.pdfbox.pdmodel.interactive.form.PDTerminalField;

import com.cuupa.converter.to.Document;

public class PDFCommand extends Command {

	private Document document;

	private List<String> blacklistedFiles = new ArrayList<String>();
	
	{
		blacklistedFiles.add(".exe");
		blacklistedFiles.add(".bat");
		blacklistedFiles.add(".js");
		blacklistedFiles.add(".py");
		blacklistedFiles.add(".app");
	}
	
	public PDFCommand(Document document) {
		this.document = document;
	}

	@Override
	public Document execute() throws IOException {
		try (PDDocument pdf = PDDocument.load(document.getContent())) {
			processDocument(pdf);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			pdf.save(byteArrayOutputStream);
			document.setContent(byteArrayOutputStream.toByteArray());
		} catch (InvalidPasswordException ipe) {
			try(PDDocument pdf = PDDocument.load(document.getContent(), "")){
				processDocument(pdf);	
			} catch(InvalidPasswordException ipe2) {
				document.setEncrypted(true);
			}
		}

		return document;
	}

	private void processDocument(PDDocument pdf) throws IOException {
		PDDocumentCatalog documentCatalog = pdf.getDocumentCatalog();
		documentCatalog = _removeEmbeddedFiles(documentCatalog);
//		documentCatalog = _removeCatalogActions(documentCatalog);
		documentCatalog = _removeFormActions(documentCatalog);
		pdf.setAllSecurityToBeRemoved(true);
	}

	private PDDocumentCatalog _removeFormActions(PDDocumentCatalog documentCatalog) throws IOException {
		PDAcroForm acroForm = documentCatalog.getAcroForm();
		if (acroForm != null) {
			List<PDField> fields = acroForm.getFields();
			for (PDField pdField : fields) {
				processField(pdField);
			}
		}
		
		return documentCatalog;
	}

	private PDDocumentCatalog _removeCatalogActions(PDDocumentCatalog documentCatalog) {
		documentCatalog.setOpenAction(null);
		documentCatalog = removeActions(documentCatalog);
		return documentCatalog;
	}

	private PDDocumentCatalog _removeEmbeddedFiles(PDDocumentCatalog documentCatalog) throws IOException {
		PDDocumentNameDictionary names = documentCatalog.getNames();
		
		if(names == null) {
			return documentCatalog;
		}
		PDEmbeddedFilesNameTreeNode embeddedFiles = names.getEmbeddedFiles();
		if(embeddedFiles == null) {
			return documentCatalog;
		}
		Map<String, PDComplexFileSpecification> map = embeddedFiles.getNames();
		
		for (Entry<String, PDComplexFileSpecification> element : map.entrySet()) {
			PDComplexFileSpecification fileSpecification = element.getValue();
			if(_isAllowedFile(fileSpecification.getEmbeddedFile().getFile().getFile())) {
				
			}
		}
		return documentCatalog;
	}

	private boolean _isAllowedFile(String file) {
		for (String blacklistedFile : blacklistedFiles) {
			return !file.endsWith(blacklistedFile);
		}
		return true;
	}

	private PDDocumentCatalog removeActions(PDDocumentCatalog documentCatalog) {
		try {
		PDDocumentCatalogAdditionalActions actions = documentCatalog.getActions();
		actions.setDP(null);
		actions.setDS(null);
		actions.setWC(null);
		actions.setWP(null);
		actions.setWS(null);
		return documentCatalog;
		} catch(Exception ex) {
			return documentCatalog;
		}
	}

	private void processField(PDField field) throws IOException {

		if (field instanceof PDTerminalField) {
			PDTerminalField termField = (PDTerminalField) field;
			PDFormFieldAdditionalActions fieldActions = field.getActions();
			if (fieldActions != null) {
				fieldActions = removeJavaScriptActions(fieldActions);
			}
			for (PDAnnotationWidget widgetAction : termField.getWidgets()) {
				PDAction action = widgetAction.getAction();
				if (action instanceof PDActionJavaScript) {
					widgetAction.setAction(null);
				} else if (action instanceof PDActionMovie) {
					widgetAction.setAction(null);
				}
			}
		}

		if (field instanceof PDNonTerminalField) {
			for (PDField child : ((PDNonTerminalField) field).getChildren()) {
				processField(child);
			}
		}
	}

	private PDFormFieldAdditionalActions removeJavaScriptActions(PDFormFieldAdditionalActions fieldActions) {
		fieldActions.setK(null);
		fieldActions.setC(null);
		fieldActions.setF(null);
		fieldActions.setV(null);
		return fieldActions;
	}

	public static void main(String[] args) throws IOException {
		new PDFCommand(null).execute();
	}

}
