package com.cuupa.converter.services;

import com.cuupa.converter.services.command.Command;
import com.cuupa.converter.services.command.CommandExecuter;
import com.cuupa.converter.services.command.EMLCommand;
import com.cuupa.converter.services.command.PDFCommand;
import com.cuupa.converter.services.command.WordCommand;
import com.cuupa.converter.to.Document;


public class Converter {

	
	public Document convert(Document document) throws Exception {
		Command command = null;
		
		if(isPdf(document)) {
			command = new PDFCommand(document);
		} else if(isEML(document)) {
			command = new EMLCommand(document);
		} else if(isWord(document)) {
			command = new WordCommand(document);
		}
		
		return CommandExecuter.execute(command);
	}

	private boolean isWord(Document document) {
		return getFileEnding(document.getFileName()).equals("docx") || getFileEnding(document.getFileName()).equals("docm");
	}

	private boolean isEML(Document document) {
		return getFileEnding(document.getFileName()).equalsIgnoreCase("eml");
	}

	private boolean isPdf(Document document) {
		return getFileEnding(document.getFileName()).equalsIgnoreCase("pdf");
	}

	private String getFileEnding(String name) {
		String[] split = name.split("\\.");
		return split[split.length - 1];
	}
}
